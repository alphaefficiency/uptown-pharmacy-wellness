<?php
// Posts Controller

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Posts extends Controller {
  
  // Get Posts by page
  public function getPostsByPage($postsPerPage , $page) {
    $catquery = new WP_Query(array(
        'post_type'      => 'post',
        'posts_per_page' => $postsPerPage,
        // Which page to display
        'paged'          => $page,
    )); 

    

    $posts = $catquery->posts;
    foreach ($posts as $post) {
      $post->photo = get_the_post_thumbnail_url($post->ID);
      $post->link = get_permalink($post->ID);
    } 
    return $posts;
  }

  // Pagination
  public function pagination($postsPerPage)  {
  // Get only published posts
  $totalPostsCounted = wp_count_posts()->publish;
  return intval(ceil($totalPostsCounted / $postsPerPage));
  }

  public function tagPagination($postsPerPage, $tagID) {
    $tag = get_tag($tagID);
    $posts = $tag->count;

    return intval(ceil($posts / $postsPerPage));
  }

  // Pagination for Post categories
  public function categoriesPagination($postsPerPage, $categoryID) {
    $args = array(
      'cat' => $categoryID,
      'post_status' => 'publish',
    );
    $the_query = new WP_Query( $args );
    return intval(ceil($the_query->found_posts/$postsPerPage));
  }

  // Shorten content if its too long
  public function getExcerptForPostContent($content, $limit) {
    $excerpt = $content;
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $exploded = explode(" " , $excerpt);
    $str = "";
    for ($i = 0; $i <= count($exploded); $i++) {
      if(strlen($str) < $limit) {
        $str = $str . " " . $exploded[$i];
      } else {
        $str = $str . ' ...';
        break;
      }
    }
    
    return $str;
  }

  // Check page number
  public function getCurrentPage($pageNumber, $totalPages) {
    $is404 = null;
    $currentPage = null;
    
    if (is_null($pageNumber)) {
      $is404 = false;
      $currentPage = 1;
    } else {
      $isNumber = intval($pageNumber);
      if ($isNumber !== null && ($pageNumber > $totalPages || $pageNumber < 1)) {
        $is404 = true;
      // Set current page to 1 if its the /blog (root) page
      } else {
        $is404 = false;
        $currentPage = $pageNumber;
      }
    }

    $response = (object) [
      'currentPage' => intval($currentPage),
      'is404' => $is404
    ];
    return $response;
  }

  /*
  *
  * Category stuff
  *
  */

  // Get Posts by page
  public function getPostsByCategoryId($postsPerPage, $currentPage, $categoryId) {
    $catquery = new WP_Query(array(
        'post_type'      => 'post',
        'posts_per_page' => $postsPerPage,
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'ID',
        // Which page to display
        'paged'          => $currentPage,
        'cat' => $categoryId
    )); 

    $posts = $catquery->posts;
    foreach ($posts as $post) {
      $post->photo = get_the_post_thumbnail_url($post->ID); // == null ? "@asset('images/home/before-form.png')" : get_the_post_thumbnail_url($post->ID);
      $post->link = get_permalink($post->ID);
    } 
    return $posts;
  }

  public function getRecommendedPosts($catID, $excludePost) {
    $posts = get_posts(array(
      'post_type' => 'post',
      'category' => $catID,
      'numberposts' => 6,
      'exclude' => array($excludePost)
    ));

    return $posts;
  }

  public function getPostsByTag($postsPerPage, $currentPage, $tagId) {
    $catquery = new WP_Query(array(
      'post_type'      => 'post',
      'posts_per_page' => $postsPerPage,
      
      // Which page to display
      'paged'          => $currentPage,
      'tag_id' => $tagId
    ));

    $posts = $catquery->posts;
    return $posts;
    }

} // END - Controller


?>