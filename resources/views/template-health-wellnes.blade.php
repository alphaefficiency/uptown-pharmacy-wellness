{{--
  Template Name: Health Wellness Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @include('pages.health.health-hero')
    @include('pages.health.health-services')
    @include('components.latest-reviews')
  @endwhile
@endsection
