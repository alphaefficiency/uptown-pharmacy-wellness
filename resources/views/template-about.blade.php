{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('pages.about-us.about-hero')
    @include('pages.about-us.about-mission')
    @include('pages.about-us.about-help')
    @include('components.latest-reviews')
  @endwhile
@endsection
