{{--
  Template Name: Thank you Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('pages.thank-you.thank-you-hero')
  @endwhile
@endsection
