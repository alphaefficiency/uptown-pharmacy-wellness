<article class="post-list__item">    
    
  <div class="post-list__content">
  <div class="post-list__thumbnail--holder">
  @if (has_post_thumbnail($post->ID))
  
  @else

    <img class="post-list__thumbnail" src="@asset('images/blog/first-blog.png')" alt="Post image" />
    <a href="{{ get_permalink($post->ID) }}">

    <svg class="post-list__image" width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="24" cy="24" r="24" fill="url(#paint0_linear_176_11871)"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M10 25.0026L38.0004 25.0026V23.001L10 23.001L10 25.0026Z" fill="white"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M22.9984 10.0005L22.9984 38L25 38L25 10.0005L22.9984 10.0005Z" fill="white"/>
      <defs>
      <linearGradient id="paint0_linear_176_11871" x1="2" y1="5.5" x2="44.5" y2="48" gradientUnits="userSpaceOnUse">
      <stop stop-color="#00FFE0"/>
      <stop offset="1" stop-color="#00BCA5"/>
      </linearGradient>
      </defs>
      </svg>
    </a>
  @endif
</div>  
  <div>

    <h2 class="post-list__heading">
      <a href="{{ get_permalink($post->ID) }}" class="post-list__heading">
        <?=Posts::getExcerptForPostContent($post->post_title, 20)?>
      </a>
      {{-- {{ Posts::getExcerptForPostTitle($post->post_title, 50) }} --}}           
      {{-- {{ ($post->post_title) }} --}}
      
    </h2>
    {{-- <hr class="post-text__hr"> --}}
    <p class="post-list__paragraph">
      @php echo Posts::getExcerptForPostContent($post->post_content, 250) @endphp         
    </p>
  </div>
</div>     
  
<div class="post-list__holder">


  <p>{{ get_the_author() }}</p>
  <p>{{ get_the_date() }}</p>

  {{-- <a class="post-text__button" href="{{ get_permalink($post->ID) }}">
      
    Read Me
    
  </a> --}}
</div>

</article>
