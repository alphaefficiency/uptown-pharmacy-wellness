<footer class="footer">
  <section class="footer__top">
    <div class="footer__top--holder">
      <a href="{{get_site_url()}}">
        <img class="footer__logo" src="@asset('images/footer/logo.svg')" alt="Footer Logo">
      </a>
      <p class="footer__location">
        3512 N Oakland Ave <br>
        Shorewood, WI 53211
      </p>
    </div>
    <div class="footer__top--links">
      <ul class="footer__navigation">
        <div class="footer__tablet">
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/about">about us</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/pharmacy">pharmacy services</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/health-wellness">health and wellness</a>
          </li>
        </div>
        <div class="footer__tablet">
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/covid">covid 19</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/blog">blog</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/contact">contact us</a>
          </li>
        </div>
        <div class="footer__mobile--holder">
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/about">about us</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/pharmacy">pharmacy services</a>
          </li>
        </div>
        <div class="footer__mobile--holder">
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/health-wellness">health and wellness</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/covid">covid 19</a>
          </li>          
        </div>
        <div class="footer__mobile--holder">
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/blog">blog</a>
          </li>
          <li class="footer__navigation--item">
            <a class="footer__navigation--link" href="{{get_site_url()}}/contact">contact us</a>
          </li>
        </div>
      </ul>
      <ul class="footer__socials footer__mobile">
        <li class="footer__socials footer__socials--item">
          <a class="footer__socials footer__socials--link" href="">
            <img class="footer__img" src="@asset('images/footer/linkedin.svg')" alt="Logo Linkedin">
          </a>
        </li>
        <li class="footer__socials footer__socials--item">
          <a class="footer__socials footer__socials--link" href="">
            <img class="footer__img" src="@asset('images/footer/twitter.svg')" alt="Logo Twitter">
          </a>
        </li>
        <li class="footer__socials footer__socials--item">
          <a class="footer__socials footer__socials--link" href="https://www.facebook.com/uptownpharmacywi/" target="_blank">
            <img class="footer__img" src="@asset('images/footer/facebook.svg')" alt="Logo Facebook">
          </a>
        </li>
        <li class="footer__socials footer__socials--item">
          <a class="footer__socials footer__socials--link" href="https://instagram.com/uptownpharmacywi" target="_blank">
            <img class="footer__img" src="@asset('images/footer/instagram.svg')" alt="Logo Instagram">
          </a>
        </li>
      </ul>
    </div>
  </section>
  <section class="footer__bottom">
    <div class="footer__bottom--left">
      <p class="footer__paragraph footer__paragraph--sub">Terms Of Use</p>
      <p class="footer__paragraph footer__paragraph--color">Privacy Policy</p>
    </div>
    <div>
      <p class="footer__paragraph">Copyright © 2022 Uptown <br> Pharmacy. All Rights Reserved</p>
    </div>
  </section>
</footer>
