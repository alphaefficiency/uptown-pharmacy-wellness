<?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 3,
        'post_status' => 'publish'
    ));
?>

<article @php post_class() @endphp>
  <header>
    {{-- <h1 class="entry-title">{!! get_the_title() !!}</h1>
    getExcerptForPostContent($content, $limit)
    @include('partials/entry-meta') 
    --}}
  </header>
  <div class="blog-hero">      
    <h1 class="blog-hero__heading--main">{!! get_the_title() !!}</h1>
    <p class="blog-hero__paragraph">@php echo Posts::getExcerptForPostContent($post->post_content, 370) @endphp</p>
  </div>
  <div class="single-blog-post">
    <div class="single-blog-post__content">
      @php the_content() @endphp
    </div>
    <div class="single-blog-post__aside">
      @include('components.posts.blog-aside')
    </div>
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
