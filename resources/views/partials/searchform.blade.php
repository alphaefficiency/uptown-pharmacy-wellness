<form action="{{get_site_url()}}" method="get" class="form-search">
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-search__input" placeholder="Keyword" />
    <div class="form-search__button">
        <input type="image" alt="Search" src="@asset('images/blog/loupe.svg')" class="form-search__buttons"   />
    </div>
    <input type="hidden" value="post" name="post_type" id="post_type" />
</form>