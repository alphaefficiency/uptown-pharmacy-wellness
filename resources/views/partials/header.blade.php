<header class="header">
  <div>
    <a class="header__logo" href="{{get_site_url()}}">
      <img src="@asset('images/header/logo.svg')" alt="Logo">
    </a>
    <a class="header__logo header__logo--mobile" href="{{get_site_url()}}">
      <img src="@asset('images/header/logo-mobile.svg')" alt="Logo">
    </a>
  </div>

  <ul class="header__navigation">
    <li class="header__item header__color header__about">
      <a href="{{get_site_url()}}/about" class="header__link">about us</a>
    </li>
    <li class="header__item header__sub">
      <div class="dropdown show">
        <a class="btn dropdown-toggle header__link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          services
          <svg class="header__arrow" width="18" height="13" viewBox="0 0 18 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1 0.999999L9 11L17 0.999999" stroke="white" stroke-width="2"/>
          </svg>                     
        </a>      
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="{{get_site_url()}}/pharmacy">PHARMACY SERVICES</a>
          <a class="dropdown-item" href="{{get_site_url()}}/health-wellness">HEALT AND WELLNESS SERVICES</a>
        </div>
      </div>
    </li>
    <li class="header__item header__color header__covid">
      <a href="{{get_site_url()}}/covid" class="header__link">covid-19</a>
    </li>
    <li class="header__item header__color header__contact">
      <a href="{{get_site_url()}}/contact" class="header__link">contact us</a>
    </li>
  </ul>
  <div class="header__hr--holder">
    <hr class="header__hr">
  </div>
  <ul class="header__navigation header__navigation--mobile">
    <li class="header__item header__item--mobile">
      <a href="{{get_site_url()}}/about" class="header__link header__link--mobile">about us</a>
    </li>
    <li class="header__item header__item--mobile">   
      <a class="header__link header__link--mobile header__link--mobile--sub">
        services
        <img class="header-mobile__arrow" src="@asset('images/header/header-arrow.svg')" alt="">          
      </a>     
    </li>
    <li class="header__item header__item--mobile header__item--drop">    
        <div>
          <a class="header__item header__item--mobile header__item--mobile--third" href="{{get_site_url()}}/pharmacy">PHARMACY SERVICES</a>
        </div>      
        <div>
          <a class="header__item header__item--mobile header__item--mobile--third" href="{{get_site_url()}}/health-wellness">HEALT AND WELLNESS SERVICES</a>
        </div>
    </li>
    <li class="header__item header__item--mobile">
      <a  href="{{get_site_url()}}/covid" class="header__link header__link--mobile">covid-19</a>
    </li>
    <li class="header__item header__item--mobile">
      <a href="{{get_site_url()}}/contact" class="header__link header__link--mobile">contact us</a>
    </li>
  </ul>
  <div class="header__hr--holder">
    <hr class="header__hr header__hr--sub">
  </div>
  <div class="header__button--holder">
    <a href="tel:+14145815396" class="header__button--mobile">call now</a>
  </div>
  <div>    
    <button class="header__hamburger">
      <img src="@asset('images/header/hamburger.svg')" alt="Hamburger">
    </button>
    <button class="header__close">
      <img src="@asset('images/header/header-close.svg')" alt="Hamburger">
    </button>
    <a href="tel:+14145815396" class="header__button">call now</a>
  </div>
</header>
<section class="overlay hidden"></section>