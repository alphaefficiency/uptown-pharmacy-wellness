<section class="latest-reviews">    
            <div class="latest-reviews__holder">
                <h1 class="latest-reviews__heading">OUR CLIENT REVIEWS</h1>
                <h1 class="latest-reviews__heading--main">WHAT OUR CLIENTS ARE SAYING</h1>
                <p class="latest-reviews__paragraph">We love hearing from our clients. Your feedback enables us to improve our service continually.</p>
                <div class="latest-reviews--button__holder latest-reviews--button__holder--mobile">
                    <a href="https://www.google.rs/maps/place/Uptown+Pharmacy+%26+Wellness/@43.0839854,-87.8815113,15.75z/data=!4m13!1m7!3m6!1s0x88051f2c262f354d:0x253687c79f02f46d!2s3512+N+Oakland+Ave,+Milwaukee,+WI+53211,+USA!3b1!8m2!3d43.0823593!4d-87.8872699!3m4!1s0x88051fdf2030e3cb:0x6538a5afb46e8d86!8m2!3d43.0824339!4d-87.8874211?hl=en&authuser=0" target="_blank" class="latest-reviews__button">View More</a>
                </div>  
            </div>  
            <div class="kokoska__holder">                
                <div class="swiper kokoska">
                    <ul class="latest-reviews__navigation swiper-wrapper">
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-first.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">KEVIN MILER</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">I recently moved into this area and needed to transfer my prescriptions promptly. Their staff was friendly and fast, and they introduced me to the number of services they offer online or via phone call.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-second.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">ANNA GARCIA</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">Uptown Pharmacy has saved my thing so many times. Their free home delivery service is really helpful when I don't have the opportunity to deliver medicine to my grandfather.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-third.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">JOHN MILER</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">I love their friendly staff - they always greet me with a smile. I rarely get to stand in a line since their service is fast.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-fourth.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">JESSICA WILSON</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">I love the atmosphere at the Uptown pharmacy; everyone is so warm and dedicated, which is a pleasant change compared to some of those big chain pharmacies.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-fifth.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">STEVEN CADDEL</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">I scheduled my Covid vaccine via an online form and received it as soon as I came to the pharmacy. Their online services are truly handy.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                        <li class="latest-reviews__item">
                            <div class="latest-reviews__header">
                                <img src="@asset('images/reviews/avatar-sixth.png')" class="latest-reviews__avatar" alt="Client profile image">
                                <h2 class="latest-reviews__title">CHARLOTTE DAVIS</h2>
                            </div>
                            <div class="latest-reviews__content">
                                <p class="latest-reviews__text">I was pleasantly surprised to see how their staff is dedicated to simplifying my medication use. Whenever I'm in doubt - I either call them or stop by and talk to their pharmacists.</p>
                            </div>
                            <img src="@asset('images/reviews/rating.svg')" class="latest-reviews__rating" alt="Client rating">
                        </li>
                    </ul>
                </div>                
            </div>     
            <div class="latest-reviews--button__holder">
                <div class="swiper-button-next">
                    <svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.953187 17.5166L9.16992 9.29987L0.953188 1.08313" stroke="white" stroke-width="2"/>
                    </svg>  
                </div>
                <a href="https://www.google.rs/maps/place/Uptown+Pharmacy+%26+Wellness/@43.0839854,-87.8815113,15.75z/data=!4m13!1m7!3m6!1s0x88051f2c262f354d:0x253687c79f02f46d!2s3512+N+Oakland+Ave,+Milwaukee,+WI+53211,+USA!3b1!8m2!3d43.0823593!4d-87.8872699!3m4!1s0x88051fdf2030e3cb:0x6538a5afb46e8d86!8m2!3d43.0824339!4d-87.8874211?hl=en&authuser=0" target="_blank" class="latest-reviews__button">View More</a>
                <div class="swiper-button-prev">  
                    <svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.0468 17.5166L1.83008 9.29987L10.0468 1.08313" stroke="white" stroke-width="2"/>
                    </svg>                      
                </div>
            </div>           
</section>