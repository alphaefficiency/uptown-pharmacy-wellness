{{-- Blog Pagination --}}

{{-- If parameters are not passed to this component print the error message --}}
@php
  if (!isset($currentPage)) {
    echo "<script>console.error('currentPage is not defined');</script>";
  }
  if (!isset($counted)) {
    echo "<script>console.error('counted is not defined');</script>";
  }
@endphp

<nav class="blog-pagination" aria-label="Blog page navigation">
  <ul class="pagination list-of-links">
    {{-- Previous link --}}
    @if($currentPage > 1)
      <li class="page-item">
        <a class="blog-pagination__link blog-pagination__link--previous" href="{{get_site_url()}}/{{$page}}?pageNumber={{$currentPage-1}}">
          {{-- <img src="@asset('images/blog/arrow-navigation.svg')" alt="preious arrow" class="page-item__previous"> --}}
          <svg class="blog-pagination__arrow blog-pagination__arrow--previous" width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.0468 17.5166L1.83008 9.29987L10.0468 1.08313" stroke="#00BCA5" stroke-width="2"/>
          </svg>            
        </a>
      </li>
    @endif

    {{-- Previous pages --}}
    @php
      $pagesToShowBeforeAndAfter = 2;
      $previousPagesArray = array();
      $start = $currentPage - $pagesToShowBeforeAndAfter;
      
      for($i = $currentPage; $i > $start; $i--) {
        array_push($previousPagesArray , $i);
      }
      $previousPagesArray = array_reverse($previousPagesArray);
    @endphp

    @for ($i = $start; $i < $currentPage; $i++)
      @if ($i > 1)
        <li class="page-item {{ $isDisabled }}">
          <a class="blog-pagination__link" href="{{get_site_url()}}/{{$page}}/?pageNumber={{$i}}">{{ $i }}</a>
        </li>
      @endif
    @endfor
    
    <li class="page-item disabled">
      <a class="blog-pagination__link" href="{{get_site_url()}}/{{$page}}/?pageNumber={{ $currentPage }}">{{ $currentPage }}</a>
    </li>

    {{-- After pages --}}
    @for ($i = $currentPage + 1; $i <= $currentPage + $pagesToShowBeforeAndAfter; $i++)
      @if ($i <= $counted)
        <li class="page-item">
          <a class="blog-pagination__link" href="{{get_site_url()}}/{{$page}}/?pageNumber={{ $i }}">{{ $i }}</a>
        </li>
      @endif
    @endfor
    {{-- Next link --}}
    @php
      $newPageNum;
        if ($currentPage < $counted):
          $newPageNum = $currentPage + 1;
    @endphp
          <li class="page-item">
            <a class="blog-pagination__link blog-pagination__link--next" href="{{get_site_url()}}/{{$page}}/?pageNumber={{$newPageNum}}">
              {{-- <img src="@asset('images/blog/arrow-navigation.svg')" alt=""> --}}
              <svg class="blog-pagination__arrow blog-pagination__arrow--next" width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1.00006 17.4336L9.2168 9.21686L1.00006 1.00012" stroke="#00BCA5" stroke-width="2"/>
              </svg>                
            </a>
          </li>
    @php
      endif;  
    @endphp
  </ul>
</nav>