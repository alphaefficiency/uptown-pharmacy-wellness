<?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 3,
        'post_status' => 'publish',
        'category'  => 1,
    ));
?>

<section class="latest-blog-page">
    <div class="latest-blog-page__holder">
        <h1 class="latest-blog-page__heading">OUR BLOG POST</h1>
        <h1 class="latest-blog-page__heading--main">WHAT’S NEW</h1>
        <p class="latest-blog-page__text">Never miss out on valuable news in the pharmacy world. Make sure to check out our blog and get the relevant information at all times.</p>
        <div class="latest-blog-page--button__holder--mobile">
            <a href="{{get_site_url()}}/blog" class="latest-blog-page__button">View More</a>
        </div>
    </div>
    <div class="swiper__holder">
        <div class="swiper mySwiper">
            <ul class="latest-blog-page__navigation swiper-wrapper">
                @foreach($recent_posts as $post)                   
                <li class="latest-blog-page__item">                        
                    <div class="latest-blog-page__top">
                        <div class="latest-blog-page__picture">
                            @if (has_post_thumbnail($post['ID']))
                            <?=get_the_post_thumbnail($post['ID'], 'full')?>             
                            @else
                            <img class="latest-blog-page__image" src="@asset('images/latest-blog/blog-post-1.png')" alt="Post image" />
                            @endif
                        </div>
                        <div class="latest-blog-page__content">
                            <a href="<?php echo get_permalink($post['ID']) ?>" class="latest-blog-page__link">                   
                                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="23.659" cy="24.3806" r="23.3875" fill="url(#paint0_linear_536_2589)"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.0161 25.3552L37.3014 25.3552V24.8064V23.9559V23.4063L10.0161 23.4062L10.0161 23.9559H10.0156V24.8064H10.0161L10.0161 25.3552Z" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.6813 10.7383L22.6813 38.0231L24.6318 38.0231L24.6318 10.7383L22.6813 10.7383Z" fill="white"/>
                                    <defs>
                                    <linearGradient id="paint0_linear_536_2589" x1="2.22044" y1="6.35279" x2="43.6358" y2="47.7681" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#00FFE0"/>
                                    <stop offset="1" stop-color="#00BCA5"/>
                                    </linearGradient>
                                    </defs>
                                </svg>  
                            </a>
                            <a href="<?php echo get_permalink($post['ID']) ?>" class="latest-blog-page__title"><?=Posts::getExcerptForPostContent($post['post_title'], 15)?></a>
                            <p class="latest-blog-page__paragraph"><?=Posts::getExcerptForPostContent($post['post_content'], 120)?></p>                        
                        </div>
                    </div>    
                    <div class="latest-blog-page__bottom">
                        <p class="latest-blog-page__author">{{ get_the_author($post['ID']) }}</p>
                        <p class="latest-blog-page__date">{{ get_the_date('m.d.y', $post['ID']) }}</p>
                    </div>  
                </li> 
                @endforeach
            </ul>        
        </div>
    </div>
        <div class="latest-blog-page--button__holder">
            <a href="{{get_site_url()}}/blog" class="latest-blog-page__button">View More</a>
        </div>

        
        {{-- 
        <li class="latest-blog__item">
            <div class="latest-blog__top">
                <div class="latest-blog__picture">
                    <img class="latest-blog__image" src="@asset('images/latest-blog/blog-post-1.png')" alt="">
                </div>
                <div class="latest-blog__content">
                    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="23.659" cy="24.3806" r="23.3875" fill="url(#paint0_linear_536_2589)"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.0161 25.3552L37.3014 25.3552V24.8064V23.9559V23.4063L10.0161 23.4062L10.0161 23.9559H10.0156V24.8064H10.0161L10.0161 25.3552Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M22.6813 10.7383L22.6813 38.0231L24.6318 38.0231L24.6318 10.7383L22.6813 10.7383Z" fill="white"/>
                        <defs>
                        <linearGradient id="paint0_linear_536_2589" x1="2.22044" y1="6.35279" x2="43.6358" y2="47.7681" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#00FFE0"/>
                        <stop offset="1" stop-color="#00BCA5"/>
                        </linearGradient>
                        </defs>
                    </svg>  
                    <h2 class="latest-blog__title">Lorem ipsum dolor</h2>
                    <p class="latest-blog__paragraph">Lorem ipsum dolor sit amet, adipiscing e sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet...</p>
                </div>
            </div>    
            <div class="latest-blog__bottom">
                <p class="latest-blog__author">Jhon Smith</p>
                <p class="latest-blog__date">01.02.2020</p>
            </div>        
        </li>
        <li class="latest-blog__item">
            <div class="latest-blog__top">                
                <div>
                    <img class="latest-blog__image" src="@asset('images/latest-blog/blog-post-2.png')" alt="">
                </div>
                <div class="latest-blog__content">
                    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="23.659" cy="24.3806" r="23.3875" fill="url(#paint0_linear_536_2589)"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.0161 25.3552L37.3014 25.3552V24.8064V23.9559V23.4063L10.0161 23.4062L10.0161 23.9559H10.0156V24.8064H10.0161L10.0161 25.3552Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M22.6813 10.7383L22.6813 38.0231L24.6318 38.0231L24.6318 10.7383L22.6813 10.7383Z" fill="white"/>
                        <defs>
                        <linearGradient id="paint0_linear_536_2589" x1="2.22044" y1="6.35279" x2="43.6358" y2="47.7681" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#00FFE0"/>
                        <stop offset="1" stop-color="#00BCA5"/>
                        </linearGradient>
                        </defs>
                    </svg>  
                    <h2 class="latest-blog__title">Lorem ipsum dolor</h2>
                    <p class="latest-blog__paragraph">Lorem ipsum dolor sit amet, adipiscing e sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet...</p>
                </div>
            </div>    
            <div class="latest-blog__bottom">
                <p class="latest-blog__author">Jhon Smith</p>
                <p class="latest-blog__date">01.02.2020</p>
            </div>        
        </li>
        <li class="latest-blog__item">
            <div class="latest-blog__top">
                <div>
                    <img class="latest-blog__image" src="@asset('images/latest-blog/blog-post-3.png')" alt="">
                </div>
                <div class="latest-blog__content">
                    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="23.659" cy="24.3806" r="23.3875" fill="url(#paint0_linear_536_2589)"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.0161 25.3552L37.3014 25.3552V24.8064V23.9559V23.4063L10.0161 23.4062L10.0161 23.9559H10.0156V24.8064H10.0161L10.0161 25.3552Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M22.6813 10.7383L22.6813 38.0231L24.6318 38.0231L24.6318 10.7383L22.6813 10.7383Z" fill="white"/>
                        <defs>
                        <linearGradient id="paint0_linear_536_2589" x1="2.22044" y1="6.35279" x2="43.6358" y2="47.7681" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#00FFE0"/>
                        <stop offset="1" stop-color="#00BCA5"/>
                        </linearGradient>
                        </defs>
                    </svg>  
                    <h2 class="latest-blog__title">Lorem ipsum dolor</h2>
                    <p class="latest-blog__paragraph">Lorem ipsum dolor sit amet, adipiscing e sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet...</p>
                </div>
            </div>      
            <div class="latest-blog__bottom">
                <p class="latest-blog__author">Jhon Smith</p>
                <p class="latest-blog__date">01.02.2020</p>
            </div>      
        </li>
    </ul> --}}

</section>