<?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 3,
        'post_status' => 'publish'
    ));

    $categories = get_categories(array(
      'hide_empty' => true,
      'exclude' => 1,
));

?>

<div class="blog-aside">
  <div class="blog-aside__search">
    <h2 class="blog-aside__heading">Search</h2>
    @include('partials.searchform')
  </div>  
  <div class="blog-aside__post">  
        <h2 class="blog-aside__heading">Recent Post</h2>             
          <ul class="blog-aside__navigation">  
            @foreach($recent_posts as $post)           
                <li class="blog-aside__single">

                  <div class=blog-aside__pictures">
                    @if (has_post_thumbnail($post['ID']))
                    <?=get_the_post_thumbnail($post['ID'], 'full')?>             
                    @else
                    <img class="post-thumbnail" src=@asset('images/blog/default-blog.png') alt="Post image" />
                    @endif
                  </div>
                  <div class="blog-aside__holder">
                      <a href="<?php echo get_permalink($post['ID']) ?>"  class="blog-aside__heading--main">
                          <h2 class="blog-aside__heading--main"><?=Posts::getExcerptForPostContent($post['post_title'], 20)?></h2>
                      </a>   
                      <p class="blog-aside__date">{{ get_the_date() }}</p>
                  </div>                     
                </li>                
            @endforeach
          </ul>                 
  </div>
  <h2 class="blog-aside__heading">Category</h2>
  <ul class="blog-aside__category">
    @foreach ($categories as $category)
    
    <li class="blog-aside__item">
      <a href="{{get_site_url()}}/category/{{ $category->slug }}" class="blog-aside__link">{{ $category->name }}</a>
    </li>

    @endforeach
  </ul>
  <div class="blog-aside__subscribe">
      <h2 class="blog-aside__heading blog-aside__heading--sub">Subscribes to Our Newsletters</h2>
      <p class="blog-aside__paragraph">Subscribe to receive updates and to hear what’s going on with our company!</p>

      @php echo do_shortcode('[newsletter]') @endphp
  </div>
  
</div>