{{--
  Template Name: Pharmacy Services Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('pages.pharmacy.pharmacy-hero')
    @include('pages.pharmacy.pharmacy-health')
    @include('components.latest-reviews')
  @endwhile
@endsection
