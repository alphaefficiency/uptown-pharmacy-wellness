{{--
  Template Name: Covid Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @include('pages.covid.covid-hero')
    @include('pages.covid.covid-battle')
    @include('components.latest-blog')

  @endwhile
@endsection
