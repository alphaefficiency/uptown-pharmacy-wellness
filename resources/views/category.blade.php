{{-- 
  Template Name: Blog
--}}

<?php
  $pageName = $post->post_name;
?>

@extends('layouts.app')

@section('content')
  @if(have_posts())   
    <div class="blog-wrapper">
      @php
        // Get category from slug
        if (is_single()) {
        $categories = get_the_category();
        $category = $categories[0];
        } else {
        $category = get_category(get_query_var('cat'));
        }

        $counted = $category->count;
        $categoryId = $category->cat_ID;
        $slug = $category->slug;

        // Get Posts by page
        $postsPerPage = 2;
        // Total number of pages
        $totalPages = Posts::categoriesPagination($postsPerPage, $categoryId);

        $res = Posts::getCurrentPage($_GET['pageNumber'], $totalPages);
        $currentPage = $res->currentPage;
        $is404 = $res->is404;

        // Get posts per page
        $posts = Posts::getPostsByCategoryId($postsPerPage, $currentPage, $categoryId);
      @endphp

      {{-- If user requested page that doesn't exist return 404 --}}
      @if ($is404)
        @include('404')
        @php return; @endphp
      @endif

      <div class="blog-hero">        
        {{-- <h1 class="blog-hero__heading">OUR BLOG POST</h1> --}}
        <h1 class="blog-hero__heading--main">{{ App::title()}}</h1>
        {{-- <p class="blog-hero__paragraph">We strive to provide our local clients with relevant news and information from the pharmacy world. Stay up to date through our pharmacy and wellness blog.</p> --}}
      </div>
      {{-- Display Posts --}}
      <div class="main-blog">
        <div>          
          <ul class="post-list">
            @foreach ($posts as $post)
                @component('components.posts.vacines-post', ['post' => $post]) @endcomponent
            @endforeach
          </ul>
          @component('components.pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages, 'page' => 'category/'.$slug]) @endcomponent
        </div>

        @component('components.posts.blog-aside') @endcomponent
      </div>   
  </div>
  
    {{-- Blog Pagination --}}


    {{-- If there are no posts, display this message --}}
    @else
      <p>@php _e('Sorry, no posts matched your criteria.'); @endphp</p>
  @endif

@endsection