{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @include('pages.home.home-hero')
    @include('pages.home.home-map')
    @include('pages.home.home-help')
    {{-- @include('pages.home.home-client') --}}
    {{-- @include('pages.home.home-posts') --}}
    @include('components.latest-reviews')
    @include('components.latest-blog')

  @endwhile
@endsection
