{{--
  Template Name: Schedule Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('pages.schedule.schedule-hero')
    @include('pages.schedule.schedule-calendar')
  @endwhile
@endsection
