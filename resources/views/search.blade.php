@extends('layouts.app')

@php
  $args = array("post_status" => "publish", "s" => $_GET['s'], "post_type" => "post");
  $searchPosts = get_posts( $args );
@endphp

@section('content')
<div class="blog-wrapper">
  <div class="blog-hero">        
    <h1 class="blog-hero__heading--main">{{ App::title() }}</h1>
    {{-- <p class="blog-hero__paragraph">We strive to provide our local clients with relevant news and information from the pharmacy world. Stay up to date through our pharmacy and wellness blog.</p> --}}
  </div>
<div class="main-blog">
  <div>          
    <ul class="post-list">
      @forelse ($searchPosts as $post)
         @component('components.posts.vacines-post', ['post' => $post]) @endcomponent
      @empty
        <p>asnifnasfi</p>
      @endforelse
    </ul>
    @component('components.pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages, 'page' => $blogPage]) @endcomponent
  </div>

  @component('components.posts.blog-aside') @endcomponent
</div>
</div>
@endsection
