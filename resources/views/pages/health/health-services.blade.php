<section class="pharmacy-health">
    <h2 class="covid-battle__heading pharmacy-health__heading">What we do</h2>
    <h2 class="covid-battle__heading--main pharmacy-health__heading--main">HEALTH AND WELLNES SERVICES</h2>
    <p class="covid-battle__text covid-battle__text--first pharmacy-health__text--first">Besides our regular pharmacy aid, we also provide health and wellness services to our local clients looking to obtain and maintain their well-being. Our services include highly beneficial IV hydration therapy, genetic testing and analysis, as well as flu testing and warfarin blood testing. 
    </p>
    <div id="health-holder" class="covid-battle__holder pharmacy-health__holder">        
            <a href="{{get_site_url()}}/contact" class="covid-battle__contact">Contact Us</a>
        <ul class="pharmacy-health__navigation">
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/health/flu.svg')" class="pharmacy-health__image" alt="Vaccines">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Flu Testing</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                                
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">We offer rapid flu tests for our clients who experience flu-like symptoms and evaluate their condition before recommending medications.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/health/genetic.svg')" class="pharmacy-health__image" alt="Vaccines">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Genetic Testing and Analysis</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                                
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Our pharmacogenomic tests decode your genes' information to better understand how you respond or do not respond to certain medications.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/health/warfarin.svg')" class="pharmacy-health__image" alt="Vaccines">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Warfarin Blood Testing</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                                
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph"> We conduct Warfarin blood tests to ensure a balance of the risk between blood clotting against the risk of internal bleeding.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/health/refil.svg')" class="pharmacy-health__image" alt="Vaccines">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">IV Hydration</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                                
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Uptown Pharmacy & Wellness offers IV hydration therapy to our local clients at their leisure, providing them with improved wellness, reduced recovery time, and much more.</p>
                </div>
            </li>
        </ul>
    </div>
</section>