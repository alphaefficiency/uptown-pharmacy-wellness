<section class="health-hero">
    <div class="pharmacy-hero__content">
        <h1 class="pharmacy-hero__heading">Learn About Our Service</h1>
        <h1 class="pharmacy-hero__heading--main">HOW WE CAN HELP</h1>
        <p class="pharmacy-hero__paragraph">
            We highly value a personalized approach to all of our local clients. With that in mind, we offer a variety of Health & Wellness programs to best suit your needs. Our packages start by getting to know our clients through a series of questions and assessments. We want to know your medical
        </p>
        <div class="pharmacy-hero__holder">
            <button class="pharmacy-hero__button health-hero__button-scroll">View More</button>            
        </div>
    </div>
</section>