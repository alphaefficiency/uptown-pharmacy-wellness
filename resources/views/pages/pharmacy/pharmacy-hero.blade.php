<section class="pharmacy-hero">
    <div class="pharmacy-hero__content">
        <h1 class="pharmacy-hero__heading">Learn About Our Service</h1>
        <h1 class="pharmacy-hero__heading--main">HOW WE CAN HELP</h1>
        <p class="pharmacy-hero__paragraph">
            Uptown Pharmacy & Wellness is dedicated to optimizing your medication outcome. Our team works hard to provide our local clients with compassionate and high-quality health care while keeping our services affordable. We’re proud to ensure each of our clients is well-respected and gets a personalized approach and health care solutions.
        </p>
        <div class="pharmacy-hero__holder">
            <button id="pharmacy-scroll" class="pharmacy-hero__button pharmacy-hero__button-scroll">View More</button>            
        </div>
    </div>
    {{-- <div class="pharmacy-hero__pictures">
        <img class="pharmacy-hero__image" src="@asset('images/about/hero.png')" alt="Pharmacy image">
    </div> --}}
</section>