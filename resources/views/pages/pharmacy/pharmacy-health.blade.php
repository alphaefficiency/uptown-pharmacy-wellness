<section class="pharmacy-health">
    <h2 class="covid-battle__heading pharmacy-health__heading">What we do</h2>
    <h2 class="covid-battle__heading--main pharmacy-health__heading--main">PHARMACY SERVICES</h2>
    <p class="covid-battle__text covid-battle__text--first pharmacy-health__text--first">To simplify our local clients' medication use, we've established a broad range of services, including regular prescription refills, preventive care, medication compounding, and medication therapy management (MTM). Being aware of the clients unable to visit us at our local pharmacy, we've provided free delivery for anyone in need. 

    </p>
    <div id="pharmacy-holder" class="covid-battle__holder pharmacy-health__holder">        
            <a href="{{get_site_url()}}/contact" class="covid-battle__contact">Contact Us</a>
        <ul class="pharmacy-health__navigation">
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/prescriptions.svg')" class="pharmacy-health__image" alt="Vaccines">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Refill your prescriptions</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                                
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Fill new and existing prescriptions in-store, online and by phone.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/medication.svg')" class="pharmacy-health__image" alt="Testing">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Medication Therapy Management</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                          </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Our free MTM program allows you to remain informed about your current health status, conditions and medications.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/prescriptions.svg')" class="pharmacy-health__image" alt="Testing">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Transfer your prescriptions</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                          </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Transferring your prescription has never been easier. Simply fill out a form on our website with your information or give us a call and request a transfer of your prescriptions.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/compounding.svg')" class="pharmacy-health__image" alt="Testing">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Medication Compounding</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                      
                        </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Besides offering several immunizations (shingles and travel vaccinations, pneumonia shots, and flu shots), we also provide Health Screenings that include blood sugar and pressure, HIV testing, and A1c.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/preventive.svg')" class="pharmacy-health__image" alt="Testing">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Preventive Care (Immunization)</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                          </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Besides offering several immunizations (shingles and travel vaccinations, pneumonia shots, and flu shots), we also provide Health Screenings that include blood sugar and pressure, HIV testing, and A1c.</p>
                </div>
            </li>
            <li class="pharmacy-health__item">
                <div class="pharmacy-health__item--top">
                    <img src="@asset('images/pharmacy/delivery.svg')" class="pharmacy-health__image" alt="Testing">
                    <div class="pharmacy-health__item--holder">
                        <h2 class="pharmacy-health__title">Free Delivery</h2>
                        <div class="pharmacy-health__button">
                            <svg class="pharmacy-health__arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.48437 1.00006L9.70111 9.2168L17.9178 1.00006" stroke="#00BCA5" stroke-width="2"/>
                            </svg>                          </div>
                    </div>
                </div>
                <div class="pharmacy-health__item--bottom">
                    <p class="pharmacy-health__paragraph">Besides offering several immunizations (shingles and travel vaccinations, pneumonia shots, and flu shots), we also provide Health Screenings that include blood sugar and pressure, HIV testing, and A1c.</p>
                </div>
            </li>
        </ul>
    </div>
</section>