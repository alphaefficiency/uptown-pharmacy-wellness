<section class="about-hero">
    <div class="about-hero__content">
        <h1 class="about-hero__heading">Get to know us</h1>
        <h1 class="about-hero__heading--main">WHO WE ARE</h1>
        <p class="about-hero__paragraph">Uptown Pharmacy and Wellness arose from our founder's desire to provide our local community with a reliable and trustworthy pharmacy. Our team strongly believes that everyone deserves equal access to care. Our individualized approach to our clients is one of our highest traits and a value we strive to nourish.</p>
        <div class="about-hero__holder">
            <button href="" class="about-hero__button button-scroll">View More</button>            
        </div>
    </div>
    {{-- <div class="about-hero__pictures">
        <img class="about-hero__image" src="@asset('images/about/hero.png')" alt="About-hero image">
    </div> --}}
</section>