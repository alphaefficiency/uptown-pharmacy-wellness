<section id="about-container" class="about-help">
    <div class="about-help__picture">
        <img class="about-help__img" src="@asset('images/about/about-image.png')" alt="">
    </div>
    <div class="about-help__content">
        <h2 class="about-help__heading">We’re here to help</h2>
        <h2 class="about-help__heading about-help__heading--main">WHAT CAN YOU <br>WITH US</h2>
        <ul class="about-help__navigation">
            <li class="about-help__item">
                <img class="about-help__bullet" src="@asset('images/about/bullet-list.svg')" alt="">
                <p>
                    To receive dedicated and outstanding health care
                </p>
            </li>
            <li class="about-help__item">
                <img class="about-help__bullet" src="@asset('images/about/bullet-list.svg')" alt="">
                <p>
                    To answer all your questions regarding your health conditions and the medications you use
                </p>
            </li>
            <li class="about-help__item">
                <img class="about-help__bullet" src="@asset('images/about/bullet-list.svg')" alt="">
                <p>                    
                    To obtain and maintain your well-being
                </p>
            </li>
            <li class="about-help__item">
                <img class="about-help__bullet" src="@asset('images/about/bullet-list.svg')" alt="">
                <p>
                    To ensure your rights to confidentiality, privacy and information
                </p>
            </li>
        </ul>
        <a href="" class="about-help__button">View More</a>
    </div>
</section>