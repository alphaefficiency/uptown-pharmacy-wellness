<section class="about-mission">
    <h1 class="about-mission__heading">Learn about our goals</h1>
    <h1 class="about-mission__heading--main">OUR MISSION</h1>
    <p class="about-mission__paragraph">Our mission is to bridge the gap between our local clients and their doctors by providing valuable information and assistance regarding their medications use. We make every effort to ease the medication therapy management, provide the necessary care and help our clients obtain their well-being.</p>
    <div class="about-mission__holder">       
        <a href="{{get_site_url()}}/contact" class="about-mission__button">Contact Us</a>
    </div>
</section>