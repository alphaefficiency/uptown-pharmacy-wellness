<section class="covid-hero">
    <div class="covid-hero__content">
        <h1 class="covid-hero__heading">Stay safe during the pandemic</h1>
        <h1 class="covid-hero__heading--main">HOW WE CAN HELP</h1>
        <p class="covid-hero__paragraph">Our team at Uptown Pharmacy & Wellness works hard to provide our local clients with much needed COVID-19 tests, vaccines and treatments. Learn more about how we can help you protect yourself and others during the pandemic. 
        </p>
        <div class="covid-hero__holder">
            <button class="covid-hero__button button-scroll">View More</button>
        </div>
    </div>
    {{-- <div class="covid-hero__pictures">
        <img class="covid-hero__image" src="@asset('images/covid/hero-img.png')" alt="Hero img">
    </div> --}}
</section>