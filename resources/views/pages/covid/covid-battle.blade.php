<section class="covid-battle">
    <h2 class="covid-battle__heading">Our battle against COVID-19</h2>
    <h2 class="covid-battle__heading--main">COVID - 19</h2>
    <p class="covid-battle__text covid-battle__text--first">Since the COVID-19 pandemic outbreak, we've made it a priority to provide our local clients with tests and vaccines that can lower the transmission of coronavirus. We strive to ensure enough immunization resources at all times to help our community fight the ongoing pandemic.
    </p>
    <p class="covid-battle__text">Still, please bear in mind that an online application for COVID-19 tests and vaccines is necessary to provide you with the best possible service; we still accept walk-ins when available.</p>
    <div id="covid-container" class="covid-battle__holder">        
            <a href="{{get_site_url()}}/contact" class="covid-battle__contact">Contact Us</a>
        <ul class="covid-battle__navigation">
            <li class="covid-battle__item">
                <div class="covid-battle__item--top">
                    <img src="@asset('images/covid/vaccines.svg')" class="covid-battle__image" alt="Vaccines">
                    <div>

                        <h2 class="covid-battle__title">Vaccines</h2>
                    </div>
                </div>
                <div class="covid-battle__item--bottom">
                    <div></div>
                    <a href="{{get_site_url()}}/schedule" class="covid-battle__button">Schedule an Appointment</a>
                </div>
            </li>
            <li class="covid-battle__item">
                <div class="covid-battle__item--top">
                    <img src="@asset('images/covid/testing.svg')" class="covid-battle__image" alt="Testing">
                    <div>

                        <h2 class="covid-battle__title">Testing</h2>
                    </div>
                </div>
                <div class="covid-battle__item--bottom">
                    <div></div>
                    <a href="{{get_site_url()}}/schedule" class="covid-battle__button">Schedule an Appointment</a>
                </div>
            </li>
        </ul>
    </div>
</section>