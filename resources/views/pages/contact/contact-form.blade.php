
<section class="contact-form">
    <ul class="contact-form__navigation">
        <li class="contact-form__item">
            <img class="contact-form__image" src="@asset('images/contact/home.svg')" alt="">
            <h2 class="contact-form__title">Pharmacy Location:</h2>
            <p class="contact-form__content">3512 N Oakland Ave <br>
                Shorewood, WI 53211
            </p>
        </li>
        <li class="contact-form__item">
            <img class="contact-form__image" src="@asset('images/contact/phone.svg')" alt="">
            <h2 class="contact-form__title">Phone Number:</h2>
            <ul class="contact-form__mails">
                <li class="contact-form__mail">
                    <a href="tel:+14145815396">Cell: 414-581-5396</a>
                </li>
                <li class="contact-form__mail">
                    <a href="tel:+14143720700">Phone: 414-372-0700</a>
                </li>
                <li class="contact-form__mail">Fax: 414-372-0777</li>
            </ul>
        </li>
        <li class="contact-form__item">
            <img class="contact-form__image" src="@asset('images/contact/mail.svg')" alt="">
            <h2 class="contact-form__title">Email Adress:</h2>
            <ul class="contact-form__mails">
                <li class="contact-form__mail">
                    <a href="mailto:info@uptownpharmacywi.com">info@uptownpharmacywi.com</a>
                </li>
            </ul>
        </li>
    </ul>   
    <div class="contact-form__main">        
        <div class="contact-form__holder">
            <h2 class="contact-form__heading">GET IN TOUCH</h2>
            {{-- <form action="" class="form">
                <div class="form__holder">
                    <label class="form__label" for="username">First Name</label>
                    <input type="text" name="username" id="username" class="form__input">
                </div>
                <div class="form__holder">
                    <label class="form__label" for="lastname">Last Name</label>
                    <input type="text" name="lastname" id="lastname" class="form__input">
                </div>
                <div class="form__holder">
                    <label class="form__label" for="email">Email address</label>
                    <input type="email" name="email" id="email" class="form__input">
                </div>
                <div class="form__holder form__holder--last">
                    <label class="form__label" for="message">How can we help?</label>
                    <textarea class="form__textarea" name="message" id="message"></textarea>
                </div>
                <div class="form__holder">
                    <button class="form__button">Submit</button>
                </div>
            </form> --}}
            <?=do_shortcode('[contact-form-7 id="90" title="Contact form 1"]')?>                
        </div>
            <div class="home-map__holder--map contact-form__holder--map">
                <img class="home-map__map contact-form__map" src="@asset('images/contact/map.png')" alt="Map">
                <div class="home-map__location">
                    <div class="home-map__location--holder">
                        <img class="home-map__marker" src="@asset('images/home/marker.svg')" alt="Marker map">
                    </div>
                    <div class="home-map__location--sub">
                        <p>3512 N Oakland Ave <br>
                            Shorewood, WI 53211</p>
                    </div>
                </div>
            </div>    
    </div> 
</section>