<section class="contact-hero">
    <h1 class="contact-hero__heading">We’re Here To Help</h1>
    <h1 class="contact-hero__heading--main">GET IN TOUCH</h1>
    <p class="contact-hero__paragraph">Please don't hesitate to get in touch with us if you have any questions or need additional information regarding our services. The Uptown Pharmacy team would love to hear from you.</p>
    {{-- <ul class="contact-hero__navigation">
        <li class="contact-hero__item">
            <img class="contact-hero__image" src="@asset('images/contact/home.svg')" alt="">
            <h2 class="contact-hero__title">Pharmacy Location:</h2>
            <p class="contact-hero__content">3512 N Oakland Ave <br>
                Shorewood, WI 53211</p>
        </li>
        <li class="contact-hero__item">
            <img class="contact-hero__image" src="@asset('images/contact/phone.svg')" alt="">
            <h2 class="contact-hero__title">Phone Number:</h2>
            <ul class="contact-hero__mails">
                <li class="contact-hero__mail">phone: 414-372-0700</li>
                <li class="contact-hero__mail">Fax: 414-372-0777</li>
                <li class="contact-hero__mail">Toll free: 866-435-3772</li>
            </ul>
        </li>
        <li class="contact-hero__item">
            <img class="contact-hero__image" src="@asset('images/contact/mail.svg')" alt="">
            <h2 class="contact-hero__title">Email Adress:</h2>
            <ul class="contact-hero__mails">
                <li class="contact-hero__mail">uptownexample@gmail.com</li>
                <li class="contact-hero__mail">uptownexample@gmail.com</li>
                <li class="contact-hero__mail">uptownexample@gmail.com</li>
            </ul>
        </li>
    </ul> --}}    
</section>