<section class="home-map">
    <div class="home-map__holder">
        <ul class="home-map__navigation">
            <li class="home-map__item">
                <h2 class="home-map__heading">COVID 19 - VACCINE</h2>
                <p class="home-map__paragraph">Schedule an appointment with us and get your COVID-19 vaccine to protect yourself and others during the pandemic.</p>
                <a class="home-map__button" href="{{get_site_url()}}/schedule">Schedule an Appointment</a>
                <img class="home-map__image" src="@asset('images/home/injection.svg')" alt="Injection icon">
            </li>
            <li class="home-map__item">
                <h2 class="home-map__heading">COVID 19 - TESTING</h2>
                <p class="home-map__paragraph">Uptown Pharmacy & Wellness is pleased to offer several COVID tests for your personal use, work requirements or travel.</p>
                <a class="home-map__button" href="{{get_site_url()}}/schedule">Schedule an Appointment</a>
                <img class="home-map__image" src="@asset('images/home/testing.svg')" alt="Testing icon">
            </li>
            <li class="home-map__item">
                <h2 class="home-map__heading">TRANSFER PRESCRIPTIONS</h2>
                <p class="home-map__paragraph">Fill out a simple form to provide us with the information needed to transfer your prescriptions to Uptown Pharmacy & Wellness.</p>
                <a class="home-map__button" href="{{get_site_url()}}/contact">Start Transfer</a>
                <img class="home-map__image" src="@asset('images/home/prescription.svg')" alt="Prescription icon">
            </li>
        </ul>
        <div class="home-map__content">
            <h2 class="home-map__heading home-map__heading--main">HOW TO REACH US</h2>
            <hr class="home-map__hr">
            <p class="home-map__paragraph home-map__paragraph--sub">
                Uptown Pharmacy & Wellness is located in the building at the corner of N Oakland Ave and E Edgewood Ave. Parking is available on the street or in the parking lot across the street on N Oakland Ave. We'd love to welcome you to our pharmacy / see you on weekdays from 8 AM - 8 PM or during the weekend from 8 AM - 4 PM. 
            </p>
            <div class="home-map__holder--map">
                <img class="home-map__map" src="@asset('images/home/home-map.png')" alt="Map">
                <div class="home-map__location">
                    <div class="home-map__location--holder">
                        <img class="home-map__marker" src="@asset('images/home/marker.svg')" alt="Marker map">
                    </div>
                    <div class="home-map__location--sub">
                        <p>3512 N Oakland Ave <br>
                            Shorewood, WI 53211</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>