<section class="home-help">
    <div class="home-help__picture">
        <img class="home-help__img" src="@asset('images/home/help.png')" alt="">
    </div>
    <div class="home-help__content">
        <h2 class="home-help__heading">About us</h2>
        <h2 class="home-help__heading home-help__heading--main">HOW WE CAN HELP</h2>
        <p class="home-help__paragraph">Uptown Pharmacy & Wellness is dedicated to optimizing your medication outcome. Our team works hard to provide our local clients with compassionate and high-quality health care while keeping our services affordable. We’re proud to ensure each of our clients is well-respected and gets a personalized approach and health care solutions.</p>
        <a href="{{get_site_url()}}/about" class="home-help__button">View More</a>
    </div>
</section>