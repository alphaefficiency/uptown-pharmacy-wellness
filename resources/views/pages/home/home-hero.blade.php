<section class="hero">
    <div class="hero__content">
        <h1 class="hero__heading">Welcome to Uptown</h1>
        <h1 class="hero__heading--main">NEW PHARMACY IN <br> YOUR NEIGHBORHOOD</h1>
        <p class="hero__paragraph">Uptown Pharmacy & Wellness is happy to be at our local clients' disposal. We strive to provide the residents of Shorewood, the greater Milwaukee area and beyond with outstanding and dedicated pharmacy services. Visit us or schedule your appointment online or by phone to ensure yourself the best possible medication outcome. </p>
        <div class="hero__holder">
            <a href="{{get_site_url()}}/contact" class="hero__button">Directions</a>
            <a href="{{get_site_url()}}/schedule" class="hero__button hero__button--sub">Schedule an Appointment</a>
        </div>
    </div>
    <div class="hero__pictures">
        <img class="hero__image" src="@asset('images/home/hero.png')" alt="Hero img">
    </div>
</section>