<section class="thank-you-hero">
    <div class="thank-you-hero__holder">
        <h1 class="thank-you-hero__heading">THANK YOU!</h1>
        <p class="thank-you-hero__paragraph">Thank you for filling out our application form. You will receive a confirmation email with further instructions soon.</p>
        <a href="{{get_site_url()}}" class="thank-you-hero__button">home</a>
    </div>
</section>