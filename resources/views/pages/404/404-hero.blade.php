<section class="page-404">
    <div class="page-404__holder">
        <img class="page-404__image" src="@asset('images/404/page-404.png')" alt="404">
        <h1 class="page-404__heading">PAGE NOT FOUND</h1>
        <a class="page-404__button" href="{{get_site_url()}}">home</a>
    </div>
</section>