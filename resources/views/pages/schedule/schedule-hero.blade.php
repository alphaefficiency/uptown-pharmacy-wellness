<section class="schedule-hero">
    <h1 class="schedule-hero__heading">We’re Here To Help</h1>
    <h1 class="schedule-hero__heading--main">BOOK YOUR <br> APPOINTMENT</h1>
    <p class="schedule-hero__paragraph">Select a service, date, and time that's most suitable for you. Book an appointment with Uptown Pharmacy & Wellness today.</p>
</section>