/* eslint-disable */
const btnElements = document.querySelectorAll(".pharmacy-health__button");
const bottomHolder = document.querySelectorAll(
  ".pharmacy-health__item--bottom"
);

const buttonHealth = document.querySelector(".health-hero__button-scroll");
const holderHealth = document.querySelector("#health-holder");
const arrowElement = document.querySelectorAll(".pharmacy-health__arrow");

btnElements.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    e.currentTarget.classList.toggle("active");
    e.currentTarget.firstElementChild.classList.toggle("active");
    e.currentTarget.parentElement.parentElement.nextElementSibling.classList.toggle(
      "active"
    );
  });
});

function scroll(section) {
  const coord = section.getBoundingClientRect();

  window.scrollTo({
    left: coord.left + window.pageXOffset,
    top: coord.top + window.pageYOffset,
    behavior: "smooth",
  });
}

if (buttonHealth) {
  buttonHealth.addEventListener("click", () => {
    scroll(holderHealth);
  });
}
