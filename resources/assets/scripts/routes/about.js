export default {
  init() {
    // JavaScript to be fired on the about us page
    /* eslint-disable */

    const aboutButton = document.querySelector(".about-hero__button");
    const aboutContainer = document.querySelector("#about-container");

    function scroll(section) {
      const coord = section.getBoundingClientRect();

      window.scrollTo({
        left: coord.left + window.pageXOffset,
        top: coord.top + window.pageYOffset,
        behavior: "smooth",
      });
    }

    aboutButton.addEventListener("click", () => {
      scroll(aboutContainer);
    });
  },
};
