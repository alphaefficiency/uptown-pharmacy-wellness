/* eslint-disable */

import Swiper from "swiper/swiper-bundle";

export default {
  init() {
    // JavaScript to be fired on all pages
    /* eslint-disable */

    var swiper = new Swiper(".mySwiper", {
      slidesPerView: "auto",
      spaceBetween: 30,
      grabCursor: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
    });

    var swiperTwo = new Swiper(".kokoska", {
      slidesPerView: 3,
      spaceBetween: 30,
      loop: true,
      loopFillGroupWithBlank: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        876: {
          slidesPerView: 3,
        },
        1024: {
          slidesPerView: 3,
        },
      },
    });

    const bodyWidth = document
      .querySelector("body")
      .getBoundingClientRect().width;
    const blogItem = document.querySelectorAll(".latest-blog-page__item");
    const reviewItem = document.querySelectorAll(".latest-reviews__item");
    const hamburgerElement = document.querySelector(".header__hamburger");
    const navigationElement = document.querySelector(
      ".header__navigation--mobile"
    );

    if (bodyWidth <= 1200) {
      blogItem.forEach((b) => b.classList.add("swiper-slide"));
    }

    reviewItem.forEach((r) => r.classList.add("swiper-slide"));

    const formElement = document.querySelector(".form-search__buttons");

    const overlayElement = document.querySelector(".overlay");
    const closeElement = document.querySelector(".header__close");
    const logoElement = document.querySelector(".header__logo");
    const linkElements = document.querySelectorAll(".header__link");
    const hrElements = document.querySelectorAll(".header__hr");
    const logoElementMobile = document.querySelector(".header__logo--mobile");
    const headerElement = document.querySelector(".header");
    const mobileArrow = document.querySelector(".header-mobile__arrow");
    const desktopArrow = document.querySelector(".dropdown-toggle");
    const mobileDropElement = document.querySelector(".header__item--drop");
    const buttonHolder = document.querySelector(".header__button--holder");
    const bodyElement = document.querySelector("body");

    const aboutButton = document.querySelector(".about-hero__button");
    const aboutContainer = document.querySelector("#about-container");
    const covidButton = document.querySelector(".covid-hero__button");
    const covidContainer = document.querySelector("#covid-container");

    function headerMobile() {
      headerElement.classList.add("active");
      buttonHolder.classList.add("active");
      overlayElement.classList.remove("hidden");
      logoElement.classList.add("active");
      logoElementMobile.classList.add("active");
      navigationElement.classList.add("active");
      closeElement.classList.add("active");
      bodyElement.style.overflow = "hidden";
      hamburgerElement.style.display = "none";
      hrElements.forEach((hr) => (hr.style.display = "block"));
      linkElements.forEach((li) => {
        li.classList.add("active");
      });
    }

    function hideHeaderMobile() {
      buttonHolder.classList.remove("active");
      headerElement.classList.remove("active");
      overlayElement.classList.add("hidden");
      logoElement.classList.remove("active");
      logoElementMobile.classList.remove("active");
      navigationElement.classList.remove("active");
      closeElement.classList.remove("active");
      bodyElement.style.overflow = "auto";
      hamburgerElement.style.display = "block";
      hrElements.forEach((hr) => (hr.style.display = "none"));
      linkElements.forEach((li) => {
        li.classList.remove("active");
      });
    }

    function showItemMobile() {
      mobileDropElement.classList.toggle("active");
      mobileArrow.classList.toggle("active");
    }

    function scroll(section) {
      const coord = section.getBoundingClientRect();

      window.scrollTo({
        left: coord.left + window.pageXOffset,
        top: coord.top + window.pageYOffset,
        behavior: "smooth",
      });
    }

    function showDropdown() {
      this.lastElementChild.classList.toggle("active");
      console.log(this);
    }

    hamburgerElement.addEventListener("click", headerMobile);
    closeElement.addEventListener("click", hideHeaderMobile);
    mobileArrow.addEventListener("click", showItemMobile);
    desktopArrow.addEventListener("click", showDropdown);

    if (aboutButton) {
      aboutButton.addEventListener("click", () => {
        scroll(aboutContainer);
      });
    }

    if (covidButton) {
      covidButton.addEventListener("click", () => {
        scroll(covidContainer);
      });
    }

    if (formElement) {
      formElement.addEventListener("click", (e) => {
        const form = document.querySelector(".form-search");
        const formInput = document.querySelector(".form-search__input");

        if (formInput.value) form.submit();
      });
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
