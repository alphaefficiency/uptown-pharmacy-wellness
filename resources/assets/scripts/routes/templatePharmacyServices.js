export default {
  init() {
    /* eslint-disable */
    // JavaScript to be fired on the about us page

    const buttonPharmacy = document.querySelector("#pharmacy-scroll");
    const holderPharmacy = document.querySelector("#pharmacy-holder");

    function scroll(section) {
      const coord = section.getBoundingClientRect();

      window.scrollTo({
        left: coord.left + window.pageXOffset,
        top: coord.top + window.pageYOffset,
        behavior: "smooth",
      });
    }

    if (buttonPharmacy) {
      buttonPharmacy.addEventListener("click", function () {
        scroll(holderPharmacy);
        console.log("clicked");
      });
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
